# import the necessary packages
import numpy as np
import cv2


def image_to_black(img):
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (3, 3), 0)
    edged = cv2.Canny(gray, 15, 200)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (1, 1))
    closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
    return closed


def get_square(doc):
    vocabular = {'TRMA': [115, 200, 5, 335]}
    return vocabular[doc]


def is_sign(img):
    x_start = 115
    x_end = 200
    y_start = 5
    y_end = 335
    cv2.imshow("KUSOK", img)
    cv2.waitKey(0)
    print(cv2.countNonZero(img[x_start:x_end, y_start:y_end]))
    if cv2.countNonZero(img[x_start:x_end, y_start:y_end]) > 300:
        try:
            cv2.imshow("KUSOK", img[x_start:x_end, y_start:y_end])
            cv2.waitKey(0)
        except Exception as e:
            print(e)
        return True
    return False

# load the image
image = cv2.imread(r'C:\Users\md_avidis\PycharmProjects\vnovicov-wetsignatureapi-bd65901f8d9b\PycharmProjects\pyserver\pdf_parser\images\Capture.PNG')

gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = cv2.GaussianBlur(gray, (3, 3), 0)
cv2.imshow("Gray", gray)
cv2.waitKey(0)

# print(gray.shape)
# cv2.imshow("Gray", gray[115:200, 5:335])
# cv2.waitKey(0)
#
# gray = gray[115:200, 5:335]

edged = cv2.Canny(gray, 15, 200)
cv2.imshow("Edged", edged)
cv2.waitKey(0)

# print(cv2.countNonZero(edged))


kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (12, 12))
closed = cv2.morphologyEx(edged, cv2.MORPH_CLOSE, kernel)
cv2.imshow("Closed", closed)
cv2.waitKey(0)

# find contours (i.e. the 'outlines') in the image and initialize the
(_, cnts, _) = cv2.findContours(closed.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
total = 0
for c in cnts:
    # approximate the contour
    peri = cv2.arcLength(c, True)
    approx = cv2.approxPolyDP(c, 0.02 * peri, True)
    #  'argmax', 'argmin', 'argpartition', 'argsort', 'astype', 'base', 'byteswap', 'choose', 'clip', 'compress', 'conj', 'conjugate', 'copy', 'ctypes', 'cumprod', 'cumsum', 'data', 'diagonal', 'dot', 'dtype', 'dump', 'dumps', 'fill', 'flags', 'flat', 'flatten', 'getfield', 'imag', 'item', 'itemset', 'itemsize', 'max', 'mean', 'min', 'nbytes', 'ndim', 'newbyteorder', 'nonzero', 'partition', 'prod', 'ptp', 'put', 'ravel', 'real', 'repeat', 'reshape', 'resize', 'round', 'searchsorted', 'setfield', 'setflags', 'shape', 'size', 'sort', 'squeeze', 'std', 'strides', 'sum', 'swapaxes', 'take', 'tobytes', 'tofile', 'tolist', 'tostring', 'trace', 'transpose', 'var', 'view']

    # if the approximated contour has four points, then assume that the
    # contour is a book -- a book is a rectangle and thus has four vertices
    if len(approx) == 4:
        # print(cv2.contourArea(c))
        # cv2.drawContours(image, [approx], -1, (0, 255, 0), 4)
        x, y, w, h = cv2.boundingRect(c)
        for_sign = edged[y:y+h, x:x+w]
        if is_sign(for_sign):
            print('ESTI PODPIS')
        total += 1

print("I found {0} books in that image".format(total))
cv2.imshow("Output", image)
cv2.waitKey(0)