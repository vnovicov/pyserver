from django.apps import AppConfig


class PdfParserConfig(AppConfig):
    name = 'pdf_parser'
