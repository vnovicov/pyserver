from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
k = 1024


@csrf_exempt
def index(request):
    if request.method == "GET":
        return render(request, 'pdf_parser/index.html')
    if request.method == 'POST':
        error_description = 'No file uploaded'
        if 'multipart/form-data' in request.content_type:
            file_name = request.FILES['fileupload'].name

            json_response_model = {
                "isSigned": False,
                'hasTRMA': False,
                'hasTRMAOrderForm': False,
                'hasTRMAInformationSchedule': False,
                'hasRisk20OrderForm': False,
            }

            if 'signed' in file_name.lower():
                json_response_model['isSigned'] = True

            if file_name.endswith('TRMA.pdf'):
                resp = json_response_model
                resp['hasTRMA'] = True
                return JsonResponse(resp)

            elif file_name.endswith('TRMAOrderForm.pdf'):
                resp = json_response_model
                resp['hasTRMAOrderForm'] = True
                return JsonResponse(resp)

            elif file_name.endswith('TRMAInformationSchedule.pdf'):
                resp = json_response_model
                resp['hasTRMAInformationSchedule'] = True
                return JsonResponse(resp)

            elif file_name.endswith('Risk20OrderForm.pdf'):
                resp = json_response_model
                resp['hasRisk20OrderForm'] = True
                return JsonResponse(resp)

        return JsonResponse({
            "status": "FAIL",
            "error": error_description
        })
